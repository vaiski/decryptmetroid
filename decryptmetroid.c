/* decryptmetroid.c
 *
 * Copyright 2018 FIX94
 * Copyright 2018 Vaiski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/*
 * Original by FIX94:
 * https://gist.github.com/FIX94/7593640c5cee6c37e3b23e7fcf8fe5b7
 */

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include "decryptmetroid.h"

static int            decBit;
static unsigned char  *rPos;
static unsigned int   tmpBuf;
static size_t         position;

/* Gets read position for out_buf */
static void runDecBit (void)
{
  if(position == 0)
  {
    position  = 8;
    tmpBuf    = *rPos++;
  }
  decBit <<= 1;
  if(tmpBuf & 0x80)
    decBit |= 1;
  tmpBuf <<= 1;
  position--;
}

void decryptMetroid (unsigned char *in_buf, unsigned char **out_buf,
                     unsigned int  decLen,  unsigned char decByte,
                     unsigned int  xorLen,  unsigned int  xorVal)
{
  unsigned int  i, j, xorTmpVal;
  unsigned char *outBuf;

  /* Simple add obfuscation */
  for(i = 0; i < 0x100; i++)
  {
    in_buf[i] += decByte;
    decByte    = in_buf[i];
  }

  /* Flip the first 0x100 bytes around */
  for(i = 0; i < 0x80; i++)
  {
    in_buf[i]         ^= in_buf[0xFF - i];
    in_buf[0xFF - i]  ^= in_buf[i];
    in_buf[i]         ^= in_buf[0xFF - i];
  }

  /* Set up buffer pointers */
  rPos      = in_buf + 0x100;
  position  = 0;
  tmpBuf    = 0;
  outBuf    = malloc(decLen);
  memset(outBuf, 0, decLen);

  /* Unscramble buffer */
  for(i = 0; i < decLen; i++)
  {
    decBit = 0;
    runDecBit();
    if(decBit)
    {
      decBit = 0;
      for(j = 0; j < 8; j++)
        runDecBit();
      outBuf[i] = in_buf[decBit + 0x49];
    }
    else
    {
      decBit = 0;
      runDecBit();
      if(decBit)
      {
        decBit = 0;
        for(j = 0; j < 6; j++)
          runDecBit();
        outBuf[i] = in_buf[decBit + 9];
      }
      else
      {
        decBit = 0;
        runDecBit();
        if(decBit)
        {
          decBit = 0;
          for(j = 0; j < 3; j++)
            runDecBit();
          outBuf[i] = in_buf[decBit + 1];
        }
        else
          outBuf[i] = in_buf[decBit];
      }
    }
  }

  /* Checksum fixups */
  xorTmpVal = 0;
  for(i = 0; i < xorLen; i++)
  {
    xorTmpVal ^= outBuf[i];
    for(j = 0; j < 8; j++)
    {
      if(xorTmpVal & 1)
      {
        xorTmpVal >>= 1;
        xorTmpVal  ^= xorVal;
      }
      else
        xorTmpVal >>= 1;
    }
  }

  /* Write in calculated checksum */
  outBuf[xorLen - 1] = (xorTmpVal >> 8) & 0xFF;
  outBuf[xorLen - 2] = xorTmpVal & 0xFF;

  *out_buf = outBuf;
}
