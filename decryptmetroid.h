/* decryptmetroid.h
 *
 * Copyright 2018 Vaiski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef DECRYPTMETROID_H
#define DECRYPTMETROID_H

enum MPVersion
{
  JPN_100,  /* GM8J01 Japan 1.00  */
  KOR_148,  /* GM8E01 Korea 1.48  */
  PAL_100,  /* GM8P01 Europe 1.00 */
  USA_100,  /* GM8E01 USA 1.00    */
  USA_101,  /* GM8E01 USA 1.01    */
  USA_102,  /* GM8E01 USA 1.02    */

  MP_VERSION_LAST
};

struct MPOption
{
  const char* const option;
  const char* const title;
};

void decryptMetroid (unsigned char *in_buf, unsigned char **out_buf,
                     unsigned int  decLen,  unsigned char decByte,
                     unsigned int  xorLen,  unsigned int  xorVal);

#endif /* DECRYPTMETROID_H */
