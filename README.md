# decryptmetroid

A simple command line application for extracting the Metroid NES ROM from
Metroid Prime for the GameCube. Based on [FIX94's decryption function](
https://gist.github.com/FIX94/7593640c5cee6c37e3b23e7fcf8fe5b7).

Currently only the 1.00 and 1.02 versions of the USA and the 1.00 version of
the PAL release are supported.

If you need a similar tool for Android, see [Metroid-ROM-Extractor](
https://github.com/farmerbb/Metroid-ROM-Extractor).

## Usage

    decryptmetroid VERSION INFILE OUTFILE

Possible options for `VERSION` are:

* `J`  [GM8J01 Japan 1.00 (Unsupported)](http://redump.org/disc/5007/)
* `K`  [GM8E01 Korea 1.48 (Unsupported)](http://redump.org/disc/52062/)
* `P`  [GM8P01 Europe 1.00](http://redump.org/disc/1247/)
* `U0` [GM8E01 USA 1.00](http://redump.org/disc/1556/)
* `U1` [GM8E01 USA 1.01 (Unsupported)](http://redump.org/disc/51570/)
* `U2` [GM8E01 USA 1.02](http://redump.org/disc/1324/)

`INFILE` must be `NESemu.rel`, `NESemuP.rel`, `NESPAL60emu.rel` or
`NESPAL60emuP.rel` extracted from a supported copy of Metroid Prime using
[WIT](https://wit.wiimm.de/), [Dolphin]( https://dolphin-emu.org/) or some
other tool. `OUTFILE` is the filename of
the extracted ROM.

## Building

### GCC / Clang

    $CC -o decryptmetroid main.c decryptmetroid.c

### CL

    cl.exe /Fedecryptmetroid main.c decryptmetroid.c

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See LICENSE for more details.
