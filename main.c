/* main.c
 *
 * Copyright 2018 Vaiski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "decryptmetroid.h"

int main (int argc, char *argv[])
{
  enum MPVersion  version;
  unsigned int    romOffset, romLen, headerLen, xorLen, xorVal;
  unsigned char   decByte;
  const char      *header;

  const char iNesHeader[16] =
  {
    0x4E, 0x45, 0x53, 0x1A, 0x08, 0x00, 0x10, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
  };

  if (argc != 4)
  {
    fprintf(stderr, "Usage: %s VERSION INFILE OUTFILE\n", argv[0]);
    return EXIT_FAILURE;
  }

  /* Define version based on VERSION */
  {
    unsigned int ndx;

    /* In same order as MPVersion! */
    const struct MPOption options[MP_VERSION_LAST] =
    {
      { "J",  "GM8J01 Japan 1.00 (Unsupported)" },
      { "K",  "GM8E01 Korea 1.48 (Unsupported)" },
      { "P",  "GM8P01 Europe 1.00"              },
      { "U0", "GM8E01 USA 1.00"                 },
      { "U1", "GM8E01 USA 1.01 (Unsupported)"   },
      { "U2", "GM8E01 USA 1.02"                 }
    };

    version = MP_VERSION_LAST;

    for (ndx = 0; ndx < MP_VERSION_LAST; ndx++)
    {
      if (strcmp(argv[1], options[ndx].option) == 0)
      {
        version = ndx;
        break;
      }
    }

    if (version >= MP_VERSION_LAST)
    {
      fprintf(stderr, "%s is not a valid game version\n", argv[1]);
      fprintf(stderr, "Possible options are:\n");

      for (ndx = 0; ndx < MP_VERSION_LAST; ndx++)
        fprintf(stderr, "  %s\t%s\n", options[ndx].option, options[ndx].title);

      return EXIT_FAILURE;
    }
  }

  /* Define decryption parameters and header based on version */
  switch (version)
  {
    case JPN_100:
    {
      romOffset = 0x0;      /* TODO: Missing offset for JPN_100 */
      romLen    = 0x1AB00;
      xorLen    = 0x1AAFC;
      xorVal    = 0xB1B2;
      decByte   = 0x9D;
      header    = NULL;
      headerLen = 0;
      fprintf(stderr, "Unsupported version of Metroid Prime\n");
      return EXIT_FAILURE;
    }
    case USA_100:
    {
      romOffset = 0xA3F8;
      romLen    = 0x20000;
      xorLen    = 0x1FFFC;
      xorVal    = 0xA663;
      decByte   = 0xE9;
      header    = iNesHeader;
      headerLen = 16;
      break;
    }
    case USA_102:
    {
      romOffset = 0xA418;
      romLen    = 0x20000;
      xorLen    = 0x1FFFC;
      xorVal    = 0xA663;
      decByte   = 0xE9;
      header    = iNesHeader;
      headerLen = 16;
      break;
    }
    case PAL_100:
    {
      romOffset = 0xAB20;
      romLen    = 0x20000;
      xorLen    = 0x1FFFC;
      xorVal    = 0xA663;
      decByte   = 0xE9;
      header    = iNesHeader;
      headerLen = 16;
      break;
    }
    default:
    {
      fprintf(stderr, "Unsupported version of Metroid Prime\n");
      return EXIT_FAILURE;
    }
  }

  /* Read INFILE, decrypt, write OUTFILE */
  {
    FILE          *inFile, *outFile;
    unsigned char *inBuf, *outBuf;

    inFile = fopen(argv[2], "rb");

    if (!inFile)
    {
      fprintf(stderr, "Couldn't open %s for reading\n", argv[2]);
      return EXIT_FAILURE;
    }

    outFile = fopen(argv[3], "wb");

    if (!outFile)
    {
      fprintf(stderr, "Couldn't open %s for writing\n", argv[3]);
      fclose(inFile);
      return EXIT_FAILURE;
    }

    inBuf = malloc(romLen);
    fseek(inFile, romOffset, SEEK_SET);
    fread(inBuf, romLen, 1, inFile);

    decryptMetroid(inBuf, &outBuf, romLen, decByte, xorLen, xorVal);

    if (header && headerLen)
      fwrite(header, 1, headerLen, outFile);

    fwrite(outBuf, romLen, 1, outFile);

    fclose(inFile);
    fclose(outFile);
    free(inBuf);
    free(outBuf);
  }

  return EXIT_SUCCESS;
}
